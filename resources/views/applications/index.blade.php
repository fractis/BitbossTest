@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Elenco candidature</h1>
        </div>
    </section>
    <section>
        <div class="container">

            <applications inline-template>
                <div>
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>{{__("First Name")}}</th>
                            <th>{{__("Last Name")}}</th>
                            <th>{{__("Actions")}}</th>
                          </tr>
                        </thead>
                        <tbody>                        
                            <tr v-for="(application, index) in items" :key="application.id">
                                <td>@{{ application.first_name }}</td>
                                <td>@{{ application.last_name }}</td>
                                <td>
                                    <a class="btn btn-success" @click="accept(application)" v-if="application.application_state.code!='ACCEPTED'">{{__("Approve")}}</a>
                                    <a class="btn btn-danger" @click="refuse(application)" v-if="application.application_state.code!='REFUSED'">{{__("Refuse")}}</a></td>
                            </tr>
                        </tbody> 
                    </table>

                    <paginator :dataSet="dataSet" @changed="fetch"></paginator>
                </div>
            </applications>
        </div>
    </section>

@endsection
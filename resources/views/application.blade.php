@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <img src="//bitboss.it/images/BitBoss/logo-black-200.png"/>
        <div class="container">
            <h1>La tua candidatura</h1>
            <p class="lead">Esito</p>
        </div>
    </section>
    <section>
        <div class="container">

            <div class="container">

                <div class="alert alert-info">{{ $application->application_state->label }}</div>

                {!! Form::open()!!}
                <div class="form-group">
                    {!! Form::label('first_name', 'Nome') !!}
                    {!! Form::text('first_name', $application->first_name, ['class' => 'form-control', 'disabled' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Cognome') !!}
                    {!! Form::text('last_name', $application->last_name, ['class' => 'form-control', 'disabled' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', $application->email, ['class' => 'form-control', 'disabled' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Telefono') !!}
                    {!! Form::text('phone', $application->phone, ['class' => 'form-control', 'disabled' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('notes', 'Note') !!}
                    {!! Form::textarea('notes', $application->notes, ['rows'=>3, 'class' => 'form-control', 'disabled' => true]) !!}
                </div>

                {!! Form::close() !!}

    </section>

@stop

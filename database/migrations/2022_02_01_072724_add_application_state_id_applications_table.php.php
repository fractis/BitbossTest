<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplicationStateIdApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('applications','application_state_id') ){
            Schema::table('applications', function (Blueprint $table) {
                $table->integer('application_state_id')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('applications','application_state_id') ){
            Schema::table('applications', function (Blueprint $table) {
                $table->dropColumn('application_state_id');
            });
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {            
        foreach (range(1, 20) as $index) {

            $name = str_random(10);

            DB::table('applications')->insert([
                'first_name' => str_random(10),
                'last_name' => $name,
                'email' => $name.'@gmail.com',
                'phone' => random_int(10000,99999),
                'user_id' => $index
            ]);
        }   
    }
}

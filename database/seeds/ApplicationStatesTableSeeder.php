<?php

use Illuminate\Database\Seeder;

class ApplicationStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('application_states')->insert([
            'id' => 1,
            'code' => 'SEND',
            'label' => 'Send'
        ]);
        DB::table('application_states')->insert([
            'id' => 2,
            'code' => 'ACCEPTED',
            'label' => 'Accepted'
        ]);
        DB::table('application_states')->insert([
            'id' => 3,
            'code' => 'REFUSED',
            'label' => 'Refused'
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Application;
use App\Models\User;

class PagesController extends Controller
{
    protected $redirectTo = '/home';

    public function homepage() {
        return view('home');
    }

    public function apply() {
        return view('apply');
    }

    public function postApply() {

        $request = request();
        $validated = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|max:255'
        ]);
       
        $application = new Application();
        $application->user_id = Auth::id();
        $application->first_name = $request->get('first_name');
        $application->last_name = $request->get('last_name');
        $application->email = $request->get('email');
        $application->phone = $request->get('phone');
        $application->notes = $request->get('notes');
        $application->application_state_id = 1; // SEND

        $application->save();

        /*
         * Notifica quando c’è una nuova candidatura (su Slack + Email agli utenti admin)
         */
        $users = User::where("is_admin", true)->get();
        foreach ($users as $user) {
            $user->sendNewApplication($user, $application);
        }

        return redirect('application')->with('success', __('Application send') );;
    } 

    public function application() {

        $application = new Application();
        $results = $application->getByUserId(Auth::id());

        if(empty($results))
            return view('apply');
        else
            return view('application', ['application' => $results]);
    }
}

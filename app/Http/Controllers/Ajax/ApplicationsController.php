<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Application;

class ApplicationsController extends Controller
{
    public function __construct()
    {
        // Accessible only to admins
    }
    
    public function acceptApplication(Request $request)
    {
        $id = $request->get('id');
        $application = new Application();
        $application->setAccept($id);

        $this->_sendNotification($id);

        return response()->json(true);
    }  
    
    public function refuseApplication(Request $request)
    {
        $id = $request->get('id');
        $application = new Application();
        $application->setRefuse($id);

        $this->_sendNotification($id);

        return response()->json(true);
    }    

    /*
     * Notifica al candidato quando viene accettato o rifiutato
     */
    private function _sendNotification($application_id) {
        $application = Application::find($application_id)->with(['application_state'])->first();
        $user = \App\Models\User::find($application->user_id);
        if($user!==null)
            $user->sendUpdateStateApplication($user, $application);        
    }
}

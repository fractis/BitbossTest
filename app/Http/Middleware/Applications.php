<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Application;

class Applications
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Un utente non può candidarsi più volte
         */
        $application = new Application();
        $results = $application->getByUserId(Auth::id());
        if(!empty($results))
            return redirect("/")->with('error', __('Application already sent') );

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Solo gli utenti admin possono accedere alla manipolazione delle candidature
         */
        $user = Auth::user();
        if($user===null || !$user->is_admin)
            return redirect("/")->with('error', __('Not allowed') );

        return $next($request);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
//use App\Models\ApplicationState;

class Application extends Model
{
    protected $guarded = [];
    
    private static $state_send = 1;
    private static $state_accepted = 2;
    private static $state_refused = 3;

    public function user()
    {
        return $this->hasOne('User');
    }  

    public function application_state()
    {
        return $this->belongsTo('App\Models\ApplicationState', 'application_state_id', 'id');
    }  

    public function getByUserId($user_id) {
        return Application::where("user_id", $user_id)
                            ->with(['application_state'])
                            ->first();
    } 

    public function setAccept($id) {
        $application = Application::find($id);
        $application->application_state_id = self::$state_accepted;
        $application->save();

        return true;
    } 

    public function setRefuse($id) {
        $application = Application::find($id);
        $application->application_state_id = self::$state_refused;
        $application->save();

        return true;
    } 
}
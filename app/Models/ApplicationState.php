<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationState extends Model
{
    protected $table = 'application_states';
}

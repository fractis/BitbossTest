<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\NewApplication;
use App\Notifications\UpdateStateApplication;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendNewApplication($user, $application)
    {
        $this->notify(new NewApplication($user, $application));
    }
    
    public function sendUpdateStateApplication($user, $application)
    {
        $this->notify(new UpdateStateApplication($user, $application));
    }

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }     
/*
    public function routeNotificationForMail($notification)
    {
        return $this->getAdminMail();
    }
*/
    public function routeNotificationForSlack($notification)
    {
        return 'https://hooks.slack.com/services/...';
    }
/*
    public function getAdminMail() {

        $users = User::where("is_admin", true)->get();

        $mails = [];
        foreach ($users as $user) {
            if(!empty($user->email))
                $mails[] = $user->email;
        }
        return $mails;
    } */ 
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class NewApplication extends Notification
{
    use Queueable;

    private $_user;
    private $_application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $application)
    {
        $this->_user = $user;
        $this->_application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {        
        return (new MailMessage())
                    ->view('email.new-application', ['user' => $this->_user, 'application' => $this->_application])
                    ->subject('New application');
    }

   public function toSlack($notifiable)
   {
      $msg = "New application by ".$this->_user->name;

      return (new SlackMessage)
                    ->content($msg);
   }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }     
}
